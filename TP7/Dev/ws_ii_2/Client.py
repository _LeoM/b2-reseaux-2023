import websockets


class Client:
    def __init__(self,addr:(str,int) = None, pseudo:str = None, websocket = None, dict = None):
        if dict == None:
            self._ip = addr[0]
            self._port = addr[1]
            self._pseudo = pseudo
            self.websocket:websockets.WebSocketServerProtocol = websocket
            self._connection = True
        else:
            for key in dict:
                setattr(Client, key, dict[key])
            
    @property
    def id(self):
        return hash(f"{self._ip}:{self._port}")
    
    def simulId(ip,port):
        return hash(f"{ip}:{port}")