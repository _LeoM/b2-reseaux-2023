const r = document.getElementById("response")
const i = document.getElementById("in")
const f = document.getElementById("form")

const uri = "ws://127.0.0.1:8080"
const webSocket = new WebSocket(uri)

class Message {
    constructor(type, author, content, time){
        this.type = type
        this.author = author
        this.content = content
        this.time = time
    }

    static fromJson(json){
        return Object.assign(new Message(), json)
    }

    html(){
        if (this.type === "annonce"){
            return `
            <div class="annonce">
                <p>${this.author} ${this.content}</p>
            </div>`
        }
        if (this.type === "message"){
            return `
            <p class="pseudo">${this.author}</p>
            <div class="message">
                <p>${this.content}</p>
            </div>`
        }
    }

    str(){
        return JSON.stringify({'type': this.type,'author': this.author, 'content': this.content, 'time': this.time})
    }
}


webSocket.onopen = (event) => {
    let name = prompt("What's your name ?")
    webSocket.send(name)
};

webSocket.onmessage = (event) => {
    let msg = Message.fromJson(JSON.parse(event.data))
    r.innerHTML += msg.html()
};

f.onsubmit = function(){
    let msg = i.value
    let message = new Message("message","you", msg, "now")
    console.log(message.str())
    webSocket.send(message.str())
    r.innerHTML += `
    <div class="message your">
        <p>${msg}</p>
    </div>`
    i.value = ""
    return false
}