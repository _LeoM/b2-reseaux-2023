import websockets


class Client:
    def __init__(self, addr:(str,int), pseudo:str, websocket):
        self._ip = addr[0]
        self._port = addr[1]
        self._pseudo = pseudo
        self.websocket:websockets.WebSocketServerProtocol = websocket
        self._connection = True
    
    @property
    def id(self):
        return hash(f"{self._ip}:{self._port}")