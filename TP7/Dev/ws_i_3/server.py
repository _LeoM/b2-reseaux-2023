import asyncio
import websockets
import logging
import json
import argparse
import sys
from Client import Client
from configparser import ConfigParser

configFile = open('./config/server.conf')
config = ConfigParser(allow_no_value=True)
config.read_string(configFile.read())

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", action="store", default=int(config["DEFAULT"]["PORT"]), type=int, help="choice a port beetween : 0 and 65535")
parser.add_argument("-a", "--address", action="store", default=config["DEFAULT"]["HOST"], type=str, help="choice server host")
args = parser.parse_args()

configFile.close()

host=args.address
port=args.port

if 0 > port or port > 65535:
    print("ERROR Le port spécifié n'est pas un port possible (de 0 à 65535).")
    sys.exit(1)
if 0<= port <= 1024:
    print("ERROR Le port spécifié est un port privilégié. Spécifiez un port au dessus de 1024.")
    sys.exit(2)

logging.basicConfig(level=logging.DEBUG ,filename="./log/server.log", filemode="w", format='%(asctime)s %(levelname)s %(message)s')
logging.getLogger().addHandler(logging.StreamHandler())

CLIENTS = {}
ROOMS = {}

async def sendAll(clients, msg,exception=[]):
    exception = [x.id for x in exception]
    for a,c in clients.items():
        c:Client
        if a in exception or not c._connection:continue
        await c.websocket.send(msg)

class Message:
    def __init__():
        pass

async def handle_client_msg(websocket:websockets.WebSocketServerProtocol):
    global CLIENTS
    while True:
        try:
            data = await websocket.recv()

            client = Client(addr=(websocket.remote_address[0],websocket.remote_address[1],),pseudo=data, websocket=websocket)
            if client.id not in CLIENTS.keys():       
                logging.info(f"New connection from {client._ip}:{client._port} : {client._pseudo}") 
                CLIENTS[client.id] = client
                msg = {
                "type":"annonce",
                "author":client._pseudo,
                "content":"s'est connecté",
                "time":"time"
                }
                msg = json.dumps(msg)
            elif not CLIENTS[client.id]._connection:
                logging.info(f"New connection from {client._ip}:{client._port} : {client._pseudo}")
                client = CLIENTS[client.id]
                client._connection = True
                msg = {
                "type":"annonce",
                "author":client._pseudo,
                "content":"s'est reconnecté",
                "time":"time"
                }
                msg = json.dumps(msg)
            else :
                client = CLIENTS[client.id]
                msg = json.loads(data)
                msg["author"] = client._pseudo
                msg = json.dumps(msg)
                print(msg)
            await sendAll(CLIENTS,msg, [client])
        except websockets.ConnectionClosedOK:
            client._connection = False
            msg = {
                "type":"annonce",
                "author":client._pseudo,
                "content":"s'est déconnecté",
                "time":"time"
            }
            msg = json.dumps(msg)
            logging.info(f"{client._ip}:{client._port} : {client._pseudo} disconnected")
            await sendAll(CLIENTS,msg)
            break


async def main():
    async with websockets.serve(handle_client_msg, host, port):
        logging.info(f"Le serveur tourne sur {host}:{port}")
        await asyncio.Future()

if __name__ == "__main__":
    asyncio.run(main())


