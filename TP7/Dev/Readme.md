TP7 DEV : Websockets et databases

I. Websockets

1. First steps

[🌞 ws_i_1_server.py](./ws_i_1/server.py) et [ws_i_1_client.py](./ws_i_1/client.py)

2. Client JS

[🌞 ws_i_2_client.js](./ws_i_2/client.js)

3. Chatroom magueule

[🌞 ws_i_3_server.py](./ws_i_3/server.py) et [ws_i_3_client.{py,js}](./ws_i_3/client.js)

II. Base de données

1. Intro données

2. Redis

[🌞 ws_ii_2_server.py](./ws_ii_2/server.py)



