const r = document.getElementById("response")
const i = document.getElementById("in")

const uri = "ws://localhost:8765"
const webSocket = new WebSocket(uri)

webSocket.onopen = (event) => {
    let name = prompt("What's your name ?")
    webSocket.send(name)
    r.innerHTML += `<p>>>> ${name}</p>`
};

webSocket.onmessage = (event) => {
    r.innerHTML += `<p><<< ${event.data}</p>`
};