import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 9999))  

s.listen(1)
conn, addr = s.accept()


html_content = ""

while True:

    try:
        msg = conn.recv(50).decode()
        msg = msg.split(" ")

        if msg[0] != "GET": break
        
        file = open(f'htdocs{msg[1]}')
        html_content = file.read()
        file.close()
        
        http_response = 'HTTP/1.0 200 OK\n\n' + html_content

        conn.send(http_response.encode())


    except socket.error as err:
        print(f"Error Occured.\n{err}")
        break
    except Exception as err:
        print(err)

conn.close()
