import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 9999))  

s.listen(1)
conn, addr = s.accept()

operatorList = {
    0:"*",
    1:"+",
    2:"-"
}

while True:

    try:
        data = conn.recv(1024)
        if not data: break
        print(f"Données reçues du client : {data}")

        conn.send("Hello".encode())

        header = conn.recv(1)
        if not header:
            break

        msg_len = int.from_bytes(header[0:1],byteorder='big')
        print(f"Lecture des {msg_len} prochains octets")

        
        chunks = []

        bytes_received = 0

        while bytes_received < msg_len:
            chunk = conn.recv(min(msg_len-bytes_received, 1024))

            if not chunk: raise('Invalid chunk received bro')

            chunks.append(chunk)

            bytes_received += len(chunk)
        

    #1 header 1 operator 1len1 1/2num1 1len2 1/2num2

        operator = operatorList[int.from_bytes([chunks[0][0]], 'big')]
        chunks = [chunks[0][1:]]

        num1len = int.from_bytes([chunks[0][0]], byteorder='big')
        chunks = [chunks[0][1:]]

        num1 = int.from_bytes(chunks[0][:num1len], byteorder='big')
        chunks = [chunks[0][num1len:]]

        num2len = int.from_bytes([chunks[0][0]], byteorder='big')
        chunks = [chunks[0][1:]]

        num2 = int.from_bytes(chunks[0], byteorder='big')

        message_received = f"{num1}{operator}{num2}"
        print(f"Received from client {message_received}")

        conn.send("thx".encode())


    except socket.error as e:
        print(f"Error Occured.:\n{e}")
        break

conn.close()
