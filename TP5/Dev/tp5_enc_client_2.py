import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 9999))
s.send('Hello'.encode())

# On reçoit la string Hello
data = s.recv(1024)

# Récupération d'une string utilisateur
msg = input("Calcul à envoyer: ").strip()

operatorList = {
    "*":0,
    "+":1,
    "-":2
}

def convertBytes(value, len=1):
    try:
        b= int(value).to_bytes(len,byteorder='big')
        return b
    except OverflowError:
        return convertBytes(value, len+1)

operatorType = ""
operatorIndex = 0


if "-" in msg:
    operatorType = "-"

if "+" in msg:
    operatorType = "+"

if "*" in msg:
    operatorType = "*"


msgsplit = msg.split(operatorType)

operator = operatorList[operatorType].to_bytes(1, 'big')
num1 = convertBytes(int(msgsplit[0]))
num1len = len(num1).to_bytes(1, 'big')
num2 = convertBytes(int(msgsplit[1]))
num2len = len(num2).to_bytes(1, 'big')

operation = operator + num1len + num1 + num2len + num2

header = len(operation).to_bytes(1, 'big')
payload = header + operation

# On envoie
s.send(payload)

# Réception et affichage du résultat
s_data = s.recv(1024)
print(s_data.decode())
s.close()