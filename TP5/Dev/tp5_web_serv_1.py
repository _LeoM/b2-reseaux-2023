import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 9999))  

s.listen(1)
conn, addr = s.accept()

ok_response = "HTTP/1.0 200 OK\n\n<h1>Hello je suis un serveur HTTP</h1>"

while True:

    try:
        msg = conn.recv(1024).decode()
        
        if "GET /" in msg:
            conn.send(ok_response.encode())

    except socket.error:
        print("Error Occured.")
        break

conn.close()
