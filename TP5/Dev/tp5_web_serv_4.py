import socket
import logging

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
host = "127.0.0.1"
port = 9999
s.bind((host, port))  

SOMEONE_IS_CONNECT = False

file = 'test.log'
rfile = "/var/log/web_server/web_server.log"

logging.basicConfig(level=logging.DEBUG ,filename=rfile, filemode="w", format='%(asctime)s %(levelname)s %(message)s')
logging.getLogger().addHandler(logging.StreamHandler())

s.settimeout(60)
s.listen(1)

logging.info(f"Le serveur tourne sur {host}:{port}")

while not SOMEONE_IS_CONNECT:
    try:
        conn, addr = s.accept()
        SOMEONE_IS_CONNECT = True
    except:
        logging.warning("Aucun client depuis plus de une minute.")

logging.info(f"Un client {addr[0]} s'est connecté.")


html_content = ""

while True:

    try:
        msg = conn.recv(1024).decode()
        msg = msg.split(" ")

        if msg[0] != "GET": break

        logging.info(f"[{addr}] Request for [{' '.join(msg)}]")
        
        try:
            file = open(f'htdocs{msg[1]}')
            html_content = file.read()
            file.close()

            http_response = 'HTTP/1.0 200 OK\n\n' + html_content

            logging.info(f"Page {msg[1]} Found")
        except:
            file = open(f'htdocs/page404.html')
            html_content = file.read()
            file.close()
            http_response = 'HTTP/1.0 400 Bad Request\n\n'
            logging.warning(f"Page not {msg[1]} Found")

        conn.send(http_response.encode())       


    except socket.error as err:
        print(f"Error Occured.\n{err}")
        break
    except Exception as err:
        print(err)

conn.close()
