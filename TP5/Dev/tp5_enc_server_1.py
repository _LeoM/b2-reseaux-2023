import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 9999))  

s.listen(1)
conn, addr = s.accept()

while True:

    try:
        # On reçoit la string Hello du client
        data = conn.recv(1024)
        if not data: break
        print(f"Données reçues du client : {data}")

        conn.send("Hello".encode())

        # On reçoit le calcul du client
        header = conn.recv(1)
        if not header:
            break

        msg_len = int.from_bytes(header[0:1],byteorder='big')


        print(f"Lecture des {msg_len} prochains octets")

        chunks = []

        bytes_received = 0

        while bytes_received < msg_len:
            chunk = conn.recv(min(msg_len-bytes_received, 1024))

            if not chunk: raise('Invalid chunk received bro')

            chunks.append(chunk)

            bytes_received += len(chunk)

        message_received = b"".join(chunks).decode("utf-8")
        print(f"Received from client {message_received}")

        conn.send(str(eval(message_received)).encode())


    except socket.error:
        print("Error Occured.")
        break

conn.close()
