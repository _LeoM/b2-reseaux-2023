import socket
import logging
import json

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
host = "127.0.0.1"
port = 9999
s.bind((host, port))  


class REQUEST:
    def __init__(self, request:str):
        self._request:str = request
        self._request_tab:(str) = self._request.split("\n")
    
    @property
    def action(self):
        return self._request.split(" ")[0]
    
    @property
    def file(self):
        return self._request.split(" ")[1]
    
    @property
    def type(self):
        extension = self._request_tab[0].split(".")[1].split(" ")[0]
        match extension:
            case "html":
                return "text/html"
            case "jpg"|"jpeg":
                return "image/jpeg"
            case "png":
                return "image/png"
            case "gif":
                return "image/gif"
            case "mp4":
                return "video/mp4"
            case "pdf":
                return "application/pdf"
            case _:
                return "text/html"

class RESPONSE:
    def __init__(self):
        self._header = "HTTP/1.0"
        self._contentType = ""
        self._contentLength = 0
        self._status = "200 OK"
        self._content = ""
    
    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, newcontent):
        self._content = newcontent

    @property
    def contentType(self):
        return self._contentType

    @contentType.setter
    def contentType(self, newType):
        self._contentType = newType
    
    @property
    def contentLength(self):
        return self._contentLength
    
    @contentLength.setter
    def contentLenght(self, newLength):
        self._contentLength = newLength
    
    @property
    def status(self):
        return self._status
    
    @status.setter
    def status(self, newStatus):
        self._status = newStatus

    @property
    def encode(self):
        return str(self).encode()+self._content
    
    def __str__(self):
        return f"{self._header} {self._status} content-type: {self._contentType} content-length: {self._contentLength}\n\n"

SOMEONE_IS_CONNECT = False
IS_UP = True

file = 'test.log'
rfile = "/var/log/web_server/web_server.log"

logging.basicConfig(level=logging.DEBUG ,filename=file, filemode="w", format='%(asctime)s %(levelname)s %(message)s')
logging.getLogger().addHandler(logging.StreamHandler())

s.settimeout(60)
s.listen(1)

logging.info(f"Le serveur tourne sur {host}:{port}")

while IS_UP:
    while not SOMEONE_IS_CONNECT:
        try:
            conn, addr = s.accept()
            SOMEONE_IS_CONNECT = True
            logging.info(f"Un client {addr[0]} s'est connecté.")
        except:
            logging.warning("Aucun client depuis plus de une minute.")
            IS_UP = False
            conn.close()
            break


    while SOMEONE_IS_CONNECT:
        
        html_content = ""

        try:
            msg = conn.recv(1024).decode()

            if msg == "Bye": 
                SOMEONE_IS_CONNECT = False
                logging.info(f"Un client {addr[0]} s'est déconnecté.")

            request = REQUEST(msg)
            if request.action != "GET": continue
            response:RESPONSE = RESPONSE()


            logging.info(f"[{addr}] Request for [{msg}]")
            
            try:
                file = open(f'htdocs{request.file}', "rb")
                fileContent = file.read()
                file.close()

                response.status = "200 OK"
                response.contentType = request.type
                response.contentLenght = len(fileContent)
                response.content = fileContent

                logging.info(f"Page {request.file} Found")
            except Exception as err:
                file = open(f'htdocs/page404.html')
                fileContent = file.read()
                file.close()

                response.contentType = "text/html"
                response.contentLenght = len(fileContent)
                response.status = "400 Bad Request"
                response.content = fileContent

                logging.warning(f"Page not {request.file} Found")
                continue

            conn.send(response.encode)


        except socket.error as err:
            print(f"Error Occured.\n{err}")
            SOMEONE_IS_CONNECT = False
            break
        except Exception as err:
            print(err)
            SOMEONE_IS_CONNECT = False
            break

