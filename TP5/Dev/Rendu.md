TP5 : Coding Encoding Decoding

II. Opti calculatrice

1. Strings sur mesure

🌞 [tp5_enc_client_1.py](./tp5_enc_client_1.py)

🌞 [tp5_enc_server_1.py](./tp5_enc_server_1.py)

2. Code Encode Decode

🌞 [tp5_enc_client_2.py](./tp5_enc_client_2.py) et [tp5_enc_server_2.py](./tp5_enc_server_2.py)

III. Serveur Web HTTP

1. Serveur Web

🌞 [tp5_web_serv_1.py](./tp5_web_serv_1.py)

2. Client Web

🌞 [tp5_web_client_2.py](./tp5_web_client_2.py)

3. Délivrer des pages web

🌞 [tp5_web_serv_3.py](./tp5_web_serv_3.py)

4. Quelques logs

🌞 [tp5_web_serv_4.py](./tp5_web_serv_4.py)

5. File download

🌞 [tp5_web_serv_5.py](./tp5_web_serv_5.py)