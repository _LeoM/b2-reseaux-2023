import socket
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", action="store", default=13337, type=int, help="choice a port beetween : 0 and 65535")
args = parser.parse_args()

host=''
port=args.port

if 0 > port or port > 65535:
    print("ERROR Le port spécifié n'est pas un port possible (de 0 à 65535).")
    sys.exit(1)
if 0<= port <= 1024:
    print("ERROR Le port spécifié est un port privilégié. Spécifiez un port au dessus de 1024.")
    sys.exit(2)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host,port))

s.listen(1)

conn, addr = s.accept()

print("Un client vient de se co et son IP c'est", addr)

while True:
    try:
        data=conn.recv(1024)

        if not data: break

        message = data.decode()

    except socket.error as err:
        print(f"Error Occured.\n{err}")
        break

    if message == "meo":
        conn.sendall("Meo à toi confrère".encode())
    elif message == "waf":
        conn.sendall("ptdr t ki".encode())
    else:
        conn.sendall(b'Mes respects humble humain.')

conn.close()