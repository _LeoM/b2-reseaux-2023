import socket

host=''
port=13337

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host,port))

s.listen(1)

conn, addr = s.accept()

print("Un client vient de se co et son IP c'est", addr)

while True:
    try:
        data=conn.recv(1024)

        if not data: break

        message = data.decode()

    except socket.error as err:
        print(f"Error Occured.\n{err}")
        break

    if message == "meo":
        conn.sendall("Meo à toi confrère".encode())
    elif message == "waf":
        conn.sendall("ptdr t ki".encode())
    else:
        conn.sendall(b'Mes respects humble humain.')

conn.close()