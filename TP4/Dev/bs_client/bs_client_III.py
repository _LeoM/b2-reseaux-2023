import socket
import sys
import re
import logging

host='127.0.0.1'
port=8080

autoriz_input = '^(-|)\d{1,6}(\+|-|\*)(-|)\d{1,6}$'

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)

logging.basicConfig(level=logging.DEBUG ,filename="./bs_client.log", filemode="w", format='%(asctime)s %(levelname)s %(message)s')
logging.getLogger().addHandler(logging.StreamHandler())

try:
    s.connect((host,port))
    logging.info(f"Connexion réussi à {host}:{port}.")
except:
    logging.error(f"Impossible de se connecter au serveur {host} sur le port {port}.")
    print(f"ERROR Impossible de se connecter au serveur {host} sur le port {port}.")
    sys.exit(1)


message = input("Saisir une opération :")

if type(message) != str: raise TypeError("Str is required")
if not (re.match(autoriz_input, message)): raise ValueError("Entry not valide")




s.sendall(message.encode())
logging.info(f'Message envoyé au serveur {host} : {message}')

data=s.recv(1024)
s.close()

logging.info(f'Réponse reçue du serveur {host} : {data.decode()}')

print(f"Le serveur a répondu{repr(data.decode())}")
sys.exit()