import socket
import sys
import re
import logging

host='10.1.1.5'
port=13337

autoriz_input = '^meo$|^waf$'

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)

logging.basicConfig(level=logging.DEBUG ,filename="/var/log/bs_client/bs_client.log", filemode="w", format='%(asctime)s %(levelname)s %(message)s')
logging.getLogger().addHandler(logging.StreamHandler())

try:
    s.connect((host,port))
    logging.info(f"Connexion réussi à {host}:{port}.")
except:
    logging.error(f"Impossible de se connecter au serveur {host} sur le port {port}.")
    print(f"ERROR Impossible de se connecter au serveur {host} sur le port {port}.")
    sys.exit(1)


message = input("Que veux-tu envoyer au serveur :")

if type(message) != str: raise TypeError("Str is required")
if not (re.match(autoriz_input, message)): raise ValueError("Entry not valide")




s.sendall(message.encode())
logging.info(f'Message envoyé au serveur {host} : {message}')

data=s.recv(1024)
s.close()

logging.info(f'Réponse reçue du serveur {host} : {data.decode()}')

print(f"Le serveur a répondu{repr(data.decode())}")
sys.exit()