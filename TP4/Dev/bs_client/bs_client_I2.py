import socket
import sys

host='10.1.1.5'
port=13337

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect((host,port))
    print(f"Connecté avec succès au serveur {host} sur le port {port}")
except:
    print(f"La connection au serveur {host} sur le port {port} a échoué")




message = input("Que veux-tu envoyer au serveur :")
s.sendall(message.encode())

data=s.recv(1024)
print(data)
s.close()

print(f"Le serveur a répondu{repr(data.decode())}")
sys.exit()