import socket
import sys
import re

host='10.1.1.5'
port=13337

autoriz_input = '^meo$|^waf$'

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect((host,port))
    print(f"Connecté avec succès au serveur {host} sur le port {port}")
except:
    print(f"La connection au serveur {host} sur le port {port} a échoué")




message = input("Que veux-tu envoyer au serveur :")

if type(message) != str: raise TypeError("Str is required")
if not (re.match(autoriz_input, message)): raise ValueError("Entry not valide")




s.sendall(message.encode())

data=s.recv(1024)
print(data)
s.close()

print(f"Le serveur a répondu{repr(data.decode())}")
sys.exit()