TP4 : I'm Socketing, r u soketin ?

I. Simple bs program

1. First steps

🌞 [bs_server_I1.py](./bs_server/bs_server_I1.py)

🌞 [bs_client_I1.py](./bs_client/bs_client_I1.py)

🌞 Commandes...

```powershell
[lm33@server bs_server]$ sudo firewall-cmd --add-port=13337/tcp --permanent
success
[lm33@server bs_server]$ sudo firewall-cmd --reload
success
[lm33@server bs_server]$ sudo firewall-cmd --list-all
public (active)
[...]
  ports: 13337/tcp
[...]
[lm33@server bs_server]$ python bs_server_I1.py &
[1] 2940
[lm33@server bs_server]$ ss -lapnt | grep python
LISTEN 0      1            0.0.0.0:13337      0.0.0.0:*     users:(("python",pid=2940,fd=3))




[lm33@client bs_client]$ python bs_client_I1.py
Le serveur a répondub'Hi mate !'



[lm33@server bs_server]$ Connected by ('10.1.1.6', 33428)
Donnée reçues du client : b'Meooooo !'
```

2. User friendly

🌞 [bs_client_I2.py](./bs_client/bs_client_I2.py)

🌞 [bs_server_I2.py](./bs_server/bs_server_I2.py)

```powershell
[lm33@server bs_server]$ python bs_server_I2.py
Un client vient de se co et son IP c'est ('10.1.1.6', 43434)


[lm33@client bs_client]$ python bs_client_I2.py
Connecté avec succès au serveur 10.1.1.5 sur le port 13337
Que veux-tu envoyer au serveur :kgj
Le serveur a répondu'Mes respects humble humain.'


[lm33@server bs_server]$ python bs_server_I2.py
Un client vient de se co et son IP c'est ('10.1.1.6', 32918)


[lm33@client bs_client]$ python bs_client_I2.py
Connecté avec succès au serveur 10.1.1.5 sur le port 13337
Que veux-tu envoyer au serveur :meo
Le serveur a répondu'Meo à toi confrère'


[lm33@server bs_server]$ python bs_server_I2.py
Un client vient de se co et son IP c'est ('10.1.1.6', 44172)


[lm33@client bs_client]$ python bs_client_I2.py
Connecté avec succès au serveur 10.1.1.5 sur le port 13337
Que veux-tu envoyer au serveur :waf
Le serveur a répondu'ptdr t ki'
```



3. You say client I hear control

🌞 [bs_client_I3.py](./bs_client/bs_client_I3.py)

```powershell
[lm33@client bs_client]$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.1.5 sur le port 13337
Que veux-tu envoyer au serveur :lkjhg
Traceback (most recent call last):
  File "/home/lm33/b2-reseaux-2023/TP4/Dev/bs_client/bs_client_I3.py", line 24, in <module>
    if not (re.match(autoriz_input, message)): raise ValueError("Entry not valide")
ValueError: Entry not valide
```


II. You say dev I say good practices

1. Args

🌞 [bs_server_II1.py](./bs_server/bs_server_II1.py)

2. Logs

🌞 [bs_server_II2A.py](./bs_server/bs_server_II2A.py)

```powershell
[lm33@server bs_server]$ sudo mkdir /var/log/bs_server
[lm33@server bs_server]$ ls -al /var/log/ | grep bs_server
drwxr-xr-x.  2 root   root        6 Nov 20 10:22 bs_server
[lm33@server bs_server]$ sudo chown lm33 /var/log/bs_server
[lm33@server bs_server]$ ls -al /var/log/ | grep bs_server
drwxr-xr-x.  2 lm33   root        6 Nov 20 10:22 bs_server
```

🌞 [bs_client_II2B.py](./bs_server/bs_client_II2B.py)

```powershell
[lm33@client bs_client]$ sudo mkdir /var/log/bs_client
[lm33@client bs_client]$ ls -al /var/log/ | grep bs_client
drwxr-xr-x.  2 root   root        6 Nov 20 10:22 bs_client
[lm33@client bs_client]$ sudo chown lm33 /var/log/bs_client
[lm33@client bs_client]$ ls -al /var/log/ | grep bs_client
drwxr-xr-x.  2 lm33   root        6 Nov 20 10:22 bs_client
```

III. COMPUTE

🌞 [bs_client_III.py](./bs_client/bs_client_III.py)
🌞 [bs_server_III.py](./bs_server/bs_server_III.py)

