from sys import argv
import requests

def get_content(url):
    r = requests.get(url)
    return r.content


def write_content(content, file):
    f = open(file , "wb")
    f.write(content)
    f.close()

if __name__ == "__main__":
    if not len(argv) < 2: 
        content = get_content(argv[1])
        write_content(content, "./tmp/web_page/web_sync.txt")
