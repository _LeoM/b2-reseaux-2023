from sys import argv
import requests
import datetime

def get_content(url):
    r = requests.get(url)
    return r.content


def write_content(content, file):
    f = open(file , "wb")
    f.write(content)
    f.close()

if __name__ == "__main__":
    if not len(argv) < 2:
        start = datetime.datetime.now()
        file = open(argv[1],"r")
        urls = file.readlines()
        for url in urls:
            url = url.strip()
            name = url.split("/")[-1]
            content = get_content(url)
            write_content(content, f"./tmp/sync/{name}")
        file.close()
        print((datetime.datetime.now()-start).total_seconds())