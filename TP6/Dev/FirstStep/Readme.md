I. Faire joujou avec l'asynchrone

1. Premiers pas

[🌞 sleep_and_print.py](./sleep_and_print.py)

[🌞 sleep_and_print_async.py](./sleep_and_print_async.py)

2. Web Requests

[🌞 web_sync.py](./web_sync.py)

[🌞 web_async.py](./web_async.py)

[🌞 web_sync_multiple.py](./web_sync_multiple.py)

[🌞 web_async_multiple.py](./web_async_multiple.py)

🌞 Mesure !

Pour 6 urls :

```powershell
PS C:\Mes Dossiers\Ynov Campus\Cours\B2\Cours\Reseaux\b2-reseaux-2023\TP6\Dev\FirstStep> python .\web_sync_multiple.py .\urlFile.txt
4.137419
PS C:\Mes Dossiers\Ynov Campus\Cours\B2\Cours\Reseaux\b2-reseaux-2023\TP6\Dev\FirstStep> python .\web_async_multiple.py .\urlFile.txt
1.776501
```