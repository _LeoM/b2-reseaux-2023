import asyncio

async def f1():
    for i in range(10):
        print(i)
        await asyncio.sleep(0.5)


loop = asyncio.get_event_loop()

# on crée une liste de tasks
tasks = [
    loop.create_task(f1()),
    loop.create_task(f1()),
]

# on lance la loop jusqu'à ce que les tâches se terminent
loop.run_until_complete(asyncio.wait(tasks))
loop.close()
