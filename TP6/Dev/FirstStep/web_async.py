from sys import argv
import aiohttp
import aiofiles
import asyncio

async def get_content(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            resp = await resp.read()
            return resp


async def write_content(content, file):
    async with aiofiles.open(file , "wb") as f:
        await f.write(content)
        await f.flush()


async def main(x):
    content = await get_content(x)
    await write_content(content, "./tmp/web_page/web_async.txt")




if __name__ == "__main__":
    if not len(argv) < 2: 
        asyncio.run(main(argv[1]))
