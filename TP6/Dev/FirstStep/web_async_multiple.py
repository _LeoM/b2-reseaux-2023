from sys import argv
import aiohttp
import aiofiles
import asyncio
import datetime

async def get_content(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            resp = await resp.read()
            return resp


async def write_content(content, file):
    async with aiofiles.open(file , "wb") as f:
        await f.write(content)
        await f.flush()


async def main(file):
    file = open(argv[1],"r")
    urls = file.readlines()
    for url in urls:
        url = url.strip()
        name = url.split("/")[-1]
        content = await get_content(url)
        await write_content(content, f"./tmp/async/{name}")
    file.close()
    




if __name__ == "__main__":
    if not len(argv) < 2:
        start = datetime.datetime.now()
        asyncio.run(main(argv[1]))
        print((datetime.datetime.now()-start).total_seconds())
