II. Chat room

2. Première version

[🌞 chat_server_ii_2.py](./TP/chat_server_ii_2.py)

[🌞 chat_client_ii_2.py](./TP/chat_client_ii_2.py)

3. Client asynchrone

[🌞 chat_client_ii_3.py](./TP/chat_client_ii_3.py)

[🌞 chat_server_ii_3.py](./TP/chat_server_ii_3.py)

4. Un chat fonctionnel

[🌞 chat_server_ii_4.py](./TP/chat_server_ii_4.py)

5. Gérer des pseudos

[🌞 chat_client_ii_5.py](./TP/chat_server_ii_5.py)

6. Déconnexion

[🌞 chat_server_ii_6.py](./TP/chat_server_ii_6.py) et [chat_client_ii_6.py](./TP/chat_client_ii_6.py)


BONUS :
pip install print-color

[SERVER](./Bonus/chat_server.py)  et  [CLIENT](./Bonus/chat_client.py)