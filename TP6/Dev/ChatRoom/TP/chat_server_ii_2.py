import asyncio
import logging

logging.basicConfig(level=logging.DEBUG ,filename="./log/chat_server/chat_server.log", filemode="w", format='%(asctime)s %(levelname)s %(message)s')
logging.getLogger().addHandler(logging.StreamHandler())

host="127.0.0.1"
port="9999"


async def handle_client_msg(reader, writer):
    while True:

        data = await reader.read(1024)
        addr = writer.get_extra_info('peername')


        if data == b'':
            break

        message = data.decode()
        logging.info(f"Received {message!r} from {addr!r}")


        writer.write(f"Hello {addr[0]}:{addr[1]}".encode())

        await writer.drain()

async def main():

    server = await asyncio.start_server(handle_client_msg, host, port)

    addrs = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    logging.info(f"Le serveur tourne sur {addrs}")

    async with server:
        await server.serve_forever()

if __name__ == "__main__":
    asyncio.run(main())
