import socket
import asyncio
from aioconsole import ainput

host = "127.0.0.1"
port = 9999

SERVER_UP = False

async def async_input(writer:asyncio.StreamReader):
    global SERVER_UP
    while SERVER_UP:
        msg = await ainput("")
        if msg == "":continue
        writer.write(msg.encode())
        await writer.drain()

async def async_receive(reader:asyncio.StreamReader):
    global SERVER_UP
    while SERVER_UP:
        data = await reader.read(1024)
        if data == b'':
            print("Server closed")
            SERVER_UP = False
            break
        print(data.decode())

async def main():
    global SERVER_UP
    pseudo = input("Choisir un pseudo : ")
    reader, writer = await asyncio.open_connection(host=host, port=port)
    writer.write(f"Hello|{pseudo}".encode())
    await writer.drain()
    SERVER_UP = True
    await asyncio.gather(async_input(writer=writer), async_receive(reader=reader))
    writer.close()
    await writer.wait_closed()

if __name__=="__main__":
    asyncio.run(main())
