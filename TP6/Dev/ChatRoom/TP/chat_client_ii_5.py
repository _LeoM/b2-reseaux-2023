import socket
import asyncio
from aioconsole import ainput

host = "127.0.0.1"
port = 9999

async def async_input(writer:asyncio.StreamReader):
    while True:
        msg = await ainput("")
        if msg == "":continue
        writer.write(msg.encode())
        await writer.drain()

async def async_receive(reader:asyncio.StreamReader):
    while True:
        data = await reader.read(1024)
        print(data.decode())

async def main():
    pseudo = input("Choisir un pseudo : ")
    reader, writer = await asyncio.open_connection(host=host, port=port)
    writer.write(f"Hello|{pseudo}".encode())
    await writer.drain()
    await asyncio.gather(async_input(writer=writer), async_receive(reader=reader))
    

if __name__=="__main__":
    asyncio.run(main())
