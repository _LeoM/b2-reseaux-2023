import asyncio
import logging

logging.basicConfig(level=logging.DEBUG ,filename="./log/chat_server/chat_server.log", filemode="w", format='%(asctime)s %(levelname)s %(message)s')
logging.getLogger().addHandler(logging.StreamHandler())

host="127.0.0.1"
port="9999"

CLIENTS = {}


async def handle_client_msg(reader, writer):
    global CLIENTS
    while True:

        data = await reader.read(1024)
        addr = writer.get_extra_info('peername')


        if data == b'':
            for a,rw in CLIENTS.items():
                if a == addr:continue
                rw["w"].write(f"Annonce : {CLIENTS[addr][pseudo]} a quitté la chatroom".encode())
                await rw["w"].drain()
            del CLIENTS[addr]
            logging.info(f"{addr[0]}:{addr[1]} : {pseudo} disconnected")
            break
        
        message:str = data.decode()
        if message.split("|")[0] == "Hello" and len(message.split("|")) > 1:
            pseudo = "|".join(message.split("|")[1:])
            CLIENTS[addr] = {"r":reader,"w":writer, "pseudo":pseudo}
            logging.info(f"New connection from {addr[0]}:{addr[1]} : {pseudo}")
            for a,rw in CLIENTS.items():
                if a == addr:continue
                rw["w"].write(f"Annonce : {pseudo} a rejoint la chatroom".encode())
                await rw["w"].drain()
            continue

        logging.info(f"Message received from {addr[0]}:{addr[1]} : {message}")

        for a,rw in CLIENTS.items():
            if a == addr:continue
            rw["w"].write(f"{CLIENTS[addr]["pseudo"]} a dit : {message}".encode())
            await rw["w"].drain()

async def main():

    server = await asyncio.start_server(handle_client_msg, host, port)

    addrs = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    logging.info(f"Le serveur tourne sur {addrs}")

    async with server:
        await server.serve_forever()

if __name__ == "__main__":
    asyncio.run(main())
