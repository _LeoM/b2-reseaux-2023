# TP1 : Maîtrise réseau du poste

## I. Basics

### ☀️ Carte réseau WiFi

```powershell
PS C:\Mes Dossiers\Ynov Campus\Cours\B2\Cours\Reseaux> ipconfig /all

Carte réseau sans fil Wi-Fi 3 :

   [...]
   Adresse physique . . . . . . . . . . . : 40-ED-00-58-B9-CC
   [...]
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.135.10(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   [...]
```

Adresse Mac : ```40-ED-00-58-B9-CC``` <br>
Adresse IP : ```192.168.135.10``` <br>
Masque de sous-réseau : ```255.255.255.0   | /24```

### ☀️ Déso pas déso

Adresse réseau LAN : ```192.168.135.10``` <br>
Adresse de BroadCast : ```192.168.135.255``` <br>
Nombre d'ip disponible : ```253 (192.168.135.1 à 192.168.135.254)```

### ☀️ Hostname

```powershell
PS C:\Mes Dossiers\Ynov Campus\Cours\B2\Cours\Reseaux> hostname
LAPTOP-EV3OHFA1
```

Hostname : ```LAPTOP-EV3OHFA1```

### ☀️ Passerelle du réseau

```powershell
PS C:\Mes Dossiers\Ynov Campus\Cours\B2\Cours\Reseaux> ipconfig

[...]
Carte réseau sans fil Wi-Fi 3 :

   [...]
   Passerelle par défaut. . . . . . . . . : fe80::b010:6dff:fee6:fd1%13
                                       192.168.135.39

PS C:\Mes Dossiers\Ynov Campus\Cours\B2\Cours\Reseaux> arp -a

Interface : 192.168.135.10 --- 0xd
  Adresse Internet      Adresse physique      Type
  192.168.135.39        b2-10-6d-e6-0f-d1     dynamique
  [...]
```

Adresse IP de la passerelle du réseau : ```192.168.135.39``` <br>
Adresse Mac de la passerelle du réseau : ```b2-10-6d-e6-0f-d1```

### ☀️ Serveur DHCP et DNS

```powershell
PS C:\Mes Dossiers\Ynov Campus\Cours\B2\Cours\Reseaux> ipconfig /all

[...]
Carte réseau sans fil Wi-Fi 3 :

   [...]
   Serveur DHCP . . . . . . . . . . . . . : 192.168.135.39
   [...]
   Serveurs DNS. . .  . . . . . . . . . . : 192.168.135.39
   [...]
```

Adresse IP du serveur DHCP : ```192.168.135.39``` <br>
Adresse IP du serveur DNS : ```192.168.135.39```

### ☀️ Table de routage

```powershell
PS C:\Mes Dossiers\Ynov Campus\Cours\B2\Cours\Reseaux> netstat -r

[...]
IPv4 Table de routage
===========================================================================
Itinéraires actifs :
Destination réseau    Masque réseau  Adr. passerelle   Adr. interface Métrique
          0.0.0.0          0.0.0.0   192.168.135.39   192.168.135.10     55
          [...]
===========================================================================

[...]
```

## II. Go further

### ☀️ Hosts ?


```powershell
PS C:\> echo "1.1.1.1 b2.hello.vous" >> \Windows\System32\drivers\etc\hosts
PS C:\> ping b2.hello.vous

Envoi d’une requête 'ping' sur b2.hello.vous [1.1.1.1] avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=49 ms TTL=49
```

### ☀️ Go mater une vidéo youtube et déterminer, pendant qu'elle tourne...

```powershell
PS C:\WINDOWS\system32> netstat -anb | Select-String 216.58.214.174 -context 0,1

UDP    0.0.0.0:62837          216.58.214.174:443
   [opera.exe]
```

### ☀️ Requêtes DNS

```powershell
PS C:\>  Resolve-DnsName -Name www.ynov.com

Name                                           Type   TTL   Section    IPAddress
----                                           ----   ---   -------    ---------
[...]
www.ynov.com                                   A      300   Answer     172.67.74.226
www.ynov.com                                   A      300   Answer     104.26.11.233
www.ynov.com                                   A      300   Answer     104.26.10.233
```

Les adresses Ip correspondant à www.ynov.com : ```172.67.74.226 | 104.26.11.233 | 104.26.10.233``` 


```powershell
PS C:\WINDOWS\system32> Resolve-DnsName 174.43.238.89

Name                           Type   TTL   Section    NameHost
----                           ----   ---   -------    --------
89.238.43.174.in-addr.arpa     PTR    7200  Answer     89.sub-174-43-238.myvzw.com
```

Nom de domaine correspondant à l'IP 174.43.238.89 : ```89.sub-174-43-238.myvzw.com```

### ☀️ Hop hop hop

```powershell
PS C:\Users\macel> tracert www.ynov.com

Détermination de l’itinéraire vers www.ynov.com [2606:4700:20::681a:be9]
avec un maximum de 30 sauts :

  1     3 ms     4 ms     3 ms  2a01cb069076dd1f0000000000000075.ipv6.abo.wanadoo.fr [2a01:cb06:9076:dd1f::75]
  [...]
 16    49 ms    40 ms    39 ms  2606:4700:20::681a:be9

Itinéraire déterminé.
```

Nombre de machines : ```16```

### ☀️ IP publique

```powershell
PS C:\Users\macel> nslookup myip.opendns.com resolver1.opendns.com
Serveur :   dns.opendns.com
Address:  208.67.222.222

Réponse ne faisant pas autorité :
Nom :    myip.opendns.com
Address:  195.7.117.146
```

Ip public : ```195.7.117.146```

### ☀️ Scan réseau

```powershell
PS C:\Users\macel> nmap -sn 10.33.64.0/20
[...]
Nmap done: 4096 IP addresses (903 hosts up) scanned in 158.63 seconds
```
Machine sur le réseaux : ```903```

## III. Le requin

### ☀️ Capture ARP

[Lien vers capture ARP](./Wireshark/arp.pcap)

Filtre : ```ARP```

### ☀️ Capture DNS

[Lien vers capture DNS](./Wireshark/dns.pcap)

Filtre : ```DNS```

### ☀️ Capture TCP

[Lien vers capture TCP](./Wireshark/tcp.pcap)

Filtre : ```TCP```