from sys import argv
from lookup import lookup
from is_up import is_up
from get_ip import get_ip

params_to_file = {
    "lookup":lookup,
    "ping":is_up,
    "ip":get_ip
}

def main(arg):
    if(len(arg) < 2): return f"{__name__} need 2 arguments (function used, function args)"
    if not (arg[1] in params_to_file):
        return(f"'{arg[1]}' is not an available command. Déso.")
    return(params_to_file[arg[1]](arg[1:]))

print(main(argv))