from os import system
try:
    from psutil import net_if_addrs
except:
    system(f"pip install psutil")
    from psutil import net_if_addrs
from sys import argv

def get_ip(arg):
    if len(arg) < 2: return f"{__name__} need card name"
    try:
        return net_if_addrs()[arg[1]][1].address
    except:
        return(f"{arg[1]} is not a valide card.")


if argv[0] == "get_ip.py":
    print(get_ip(argv))