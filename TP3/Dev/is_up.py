from os import system, name
import platform
from sys import argv


def is_up(arg):
    if platform.system() == "Windows":
        option = "n"
    else:
        option = "c"

    if len(arg) < 2: return f"{__name__} need target ip"
    if (system(f"ping -{option} 1 {arg[1]}") == 0):
        return ("UP !")
    else:
        return ("DOWN !")

if argv[0] == "is_up.py":
    print(is_up(argv))