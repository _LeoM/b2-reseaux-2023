import socket
from sys import argv

def lookup(arg):
    if len(arg) < 2: return f"{__name__} need hostname"
    return socket.gethostbyname(arg[1])

if (argv[0] == "lookup.py"):
    print(lookup(argv))

