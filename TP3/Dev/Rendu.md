# TP3 DEV : Python et réseau

## I. Ping

### 🌞 ping_simple.py
[🐍Code Ping Simple](ping_simple.py)


### 🌞 ping_arg.py
[🐍Code Ping Arg](ping_arg.py)


### 🌞 is_up.py
[🐍Code Is Up](is_up.py)

## II. DNS

### 🌞 lookup.py
[🐍Code Lookup](lookup.py)

## III. Get your IP

### 🌞 get_ip.py
[🐍Code Get Ip](get_ip.py)

### 🌞 network.py
[🐍Code Network](network.py)

## V. Deploy
